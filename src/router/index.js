import Vue from 'vue'
import Router from 'vue-router'
import accueil from '@/components/accueil'
import produits from '@/components/produits'
import informations from '@/components/informations'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: accueil
    },{
      path: '/produits',
      component: produits
    },{
      path: '/informations',
      component: informations
    }
  ]
})
